from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf
from tensorflow import keras


import numpy as np
import matplotlib.pyplot as plt

print(tf.__version__)

# Data preparation with integers


# Data preparation with word embeddings 

# The Embedding layer takes at least two arguments:
# the number of possible words in the vocabulary, here 1000 (1 + maximum word index),
# and the dimensionality of the embeddings, here 32.
embedding_layer = keras.layers.Embedding(1000, 32)

vocab_size = 10000
imdb = keras.datasets.imdb
(train_data, train_labels), (test_data, test_labels) = imdb.load_data(num_words=vocab_size)

# print(train_data[0])

# A dictionary mapping words to an integer index
word_index = imdb.get_word_index()

# The first indices are reserved
word_index = {k:(v+3) for k,v in word_index.items()}
word_index["<PAD>"]    = 0
word_index["<START>"]  = 1
word_index["<UNK>"]    = 2  # unknown
word_index["<UNUSED>"] = 3

reverse_word_index = dict([(value, key) for (key, value) in word_index.items()])

def decode_review(text):
    return ' '.join([reverse_word_index.get(i, '?') for i in text])

# print(decode_review(train_data[0]))

maxlen = 500 # original was 250

train_data = keras.preprocessing.sequence.pad_sequences(train_data, value=word_index["<PAD>"],  padding='post', maxlen=maxlen)
test_data = keras.preprocessing.sequence.pad_sequences(test_data, value=word_index["<PAD>"], padding='post', maxlen=maxlen)

# print(train_data[0])

# Embedding model

embedding_dim=16

embedding_model = keras.Sequential([
  tf.keras.layers.Embedding(vocab_size, embedding_dim, input_length=maxlen),
  tf.keras.layers.GlobalAveragePooling1D(),
  tf.keras.layers.Dense(16, activation='relu'),
  tf.keras.layers.Dense(1, activation='sigmoid')
])

print(embedding_model.summary())

embedding_model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
history = embedding_model.fit(train_data, train_labels, epochs=30, batch_size=512, validation_data=(test_data, test_labels))

# Visualize 

history_dict = history.history

acc = history_dict['accuracy']
val_acc = history_dict['val_accuracy']
loss = history_dict['loss']
val_loss = history_dict['val_loss']

epochs = range(1, len(acc) + 1)

plt.figure(figsize=(12,9))
plt.plot(epochs, loss, 'bo', label='Training loss')
plt.plot(epochs, val_loss, 'b', label='Validation loss')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.show()

plt.figure(figsize=(12,9))
plt.plot(epochs, acc, 'bo', label='Training acc')
plt.plot(epochs, val_acc, 'b', label='Validation acc')
plt.title('Training and validation accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend(loc='lower right')
plt.ylim((0.5,1))
plt.show()

# Retrieve the embeddings 

e = embedding_model.layers[0]
weights = e.get_weights()[0]
print(weights.shape) # shape: (vocab_size, embedding_dim)

# TODO: The embeddings above are booty - needs word2vec
# https://towardsdatascience.com/an-implementation-guide-to-word2vec-using-numpy-and-google-sheets-13445eebd281

# conv_train_data = train_data.reshape(25000, 256, 1)
# conv_test_data  = test_data.reshape(25000, 256, 1)

# TODO: normalization 
# source: https://www.tensorflow.org/beta/tutorials/text/word_embeddings
# https://stackoverflow.com/questions/43396572/dimension-of-shape-in-conv1d/43399308#43399308

# model = tf.keras.models.Sequential()

# model.add(tf.keras.layers.Conv1D(filters=32, kernel_size=3, padding="same", activation="relu", input_shape=[256, 1]))
# model.add(tf.keras.layers.Conv1D(filters=64, kernel_size=2, padding="same", activation="relu"))
# model.add(tf.keras.layers.Conv1D(filters=128, kernel_size=1, padding="same", activation="sigmoid"))

# # model.add(tf.keras.layers.Conv2D(filters=32, kernel_size=3, padding="same", activation="relu", input_shape=[16, 16, 1]))
# # model.add(tf.keras.layers.Conv2D(filters=32, kernel_size=3, padding="same", activation="sigmoid"))
# # # model.add(tf.keras.layers.MaxPool2D(pool_size=2, strides=2, padding='valid'))
# # model.add(tf.keras.layers.Conv2D(filters=64, kernel_size=2, padding="same", activation="sigmoid"))
# # model.add(tf.keras.layers.Conv2D(filters=64, kernel_size=2, padding="same", activation="sigmoid"))
# # model.add(tf.keras.layers.MaxPool2D(pool_size=2, strides=2, padding='valid'))
# model.add(tf.keras.layers.Flatten())
# model.add(tf.keras.layers.Dense(units=128, activation='relu'))
# model.add(tf.keras.layers.Dense(units=64, activation='sigmoid'))
# model.add(tf.keras.layers.Dense(units=2, activation='softmax'))

# print(model.summary())

# model.compile(loss="sparse_categorical_crossentropy", optimizer="Adam", metrics=["sparse_categorical_accuracy"])
# train_history = model.fit(conv_train_data, train_labels, epochs=5)
# # test_loss, test_accuracy = model.evaluate(conv_train_data, test_labels)
# test_history  = model.evaluate(conv_train_data, test_labels)

# # print("Test accuracy: {}".format(test_accuracy))

# # https://machinelearningmastery.com/display-deep-learning-model-training-history-in-keras/

# # summarize history for accuracy
# # plt.plot(history.history['sparse_categorical_accuracy'])
# # # plt.plot(history.history['val_acc'])
# # plt.title('model accuracy')
# # plt.ylabel('accuracy')
# # plt.xlabel('epoch')
# # plt.legend(['train', 'test'], loc='upper left')
# # plt.show()

# # summarize history for loss
# plt.plot(train_history.history['loss'])
# plt.plot(test_history.history['loss'])
# plt.title('model loss')
# plt.ylabel('loss')
# plt.xlabel('epoch')
# plt.legend(['train', 'test'], loc='upper left')
# plt.show()


# https://missinglink.ai/guides/deep-learning-frameworks/keras-conv1d-working-1d-convolutional-neural-networks-keras/